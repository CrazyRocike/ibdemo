//
//  CustomView.swift
//  IBDemo
//
//  Created by Aici on 2019/11/6.
//  Copyright © 2019 MengQiDuoTechnology Co., Ltd. All rights reserved.
//

import UIKit

@IBDesignable
class CustomView: UIView {

    @IBOutlet weak var headerImg: UIImageView!
    @IBOutlet var contentView: CustomView!
    
    static var isLoad:Bool = false /// 用来打破递归循环
    
    @IBInspectable var imgRadius:Float = 0.0 {
        didSet{
            headerImg.layer.masksToBounds = true
            headerImg.layer.cornerRadius = CGFloat(imgRadius)
        }
    }
    
    @IBInspectable var viewRadius:Float = 0.0 {
        didSet{
            layer.masksToBounds = true
            layer.cornerRadius = CGFloat(imgRadius)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        initialFromXib()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initialFromXib()
        
    }
    
    @IBAction func changeAction(_ sender: UIButton) {
        if sender.isSelected {
            sender.isSelected = false
            self.headerImg.image = UIImage(named: "nezha")
        }else{
            sender.isSelected = true
            self.headerImg.image = UIImage(named: "yeah")
        }
        
        
    }
    
    
    func initialFromXib() {
        if CustomView.isLoad {
            return
        }else{
            CustomView.isLoad = true
        }
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: "CustomView", bundle: bundle)
        contentView = (nib.instantiate(withOwner: self, options: nil)[0] as? UIView as! CustomView)
        CustomView.isLoad = false
        contentView.frame = self.bounds
        self.addSubview(contentView)
    }
}
